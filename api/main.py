import pymongo
from flask import Flask
from flask_restful import Resource, Api
from pymongo import MongoClient
from bson.json_util import dumps
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api
from classes import *

app = Flask(__name__)
api = Api(app)
client = MongoClient()
client = MongoClient('mongodb://localhost:27017/db1')
db = client.db1
cors = CORS(app, resources={r"/*": {"origins": "*"}})


api.add_resource(HelloWorld, '/')
api.add_resource(Users, '/users')

if __name__ == '__main__':
    app.run(debug=True, port=8080)

