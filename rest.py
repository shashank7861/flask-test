import pymongo
from flask import Flask, render_template, url_for, json, jsonify, request
from flask_restful import Resource, Api
from pymongo import MongoClient
from bson.json_util import dumps
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
  def get(self):
    x = {'hello': 'world'}
    return x

api.add_resource(HelloWorld, '/')

if __name__ == '__main__':
    app.run(debug=True, port=8080)

