from flask import Flask, render_template, url_for, json, jsonify, request
# from flask.ext.cors import CORS, cross_origin
import pymongo
from pymongo import MongoClient
from bson.json_util import dumps
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api

import os
from werkzeug import check_password_hash, generate_password_hash

app = Flask(__name__)
CORS(app, support_credentials=True)
api = Api(app)

app.config['CORS_HEADERS'] = 'application/json'
client = MongoClient()

client = MongoClient('mongodb://localhost:27017/db1')
db = client.db1
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# @app.route('/')
# def api_root():
#     return 'Welcome'

@app.route('/api/users')
def showjson():
    # SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    # json_url = os.path.join(SITE_ROOT, "./", "data.json")
    data = dumps(db.users.find({})) #Getting data from MongoDB
    # data = mongo.db.users.find({})
    # data = json.load(open(json_url))
    print (data)
    return (data), 200

@app.route('/api/getUsers')
@cross_origin(supports_credentials=True)
def getusers():
    if request.method == 'GET':
        query = request.args
        data = db.users.find_one(query)
        print (data)
        return jsonify(data), 200

@app.route('/api/postUser', methods=['POST'])
@cross_origin(supports_credentials=True)
def user():
    # if request.method == 'GET':
    #     query = request.args
    #     data = mongo.db.users.find_one(query)
    #     return jsonify(data), 200
    print(request)
    data = request.get_json()
    print(data)
    if request.method == 'POST':
        if data.get('name', None) is not None and data.get('email', None) is not None:
            db.users.insert_one(data)
            return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400
# # def api_articles():
# #     return 'List of ' + url_for('api_articles')

# # @app.route('/articles/<articleid>')
# # def api_article(articleid):
# #     return 'You are reading ' + articleid

@app.route('/api/userSingup', methods=['POST'])
@cross_origin(supports_credentials=True)
def userSingup():
    data = request.json
    # _name = request.name
    # _email = request.email
    # _password = request.password
    # print(request)
    # print(data['email'])
    user = db.userLogin.find_one({'email': data['email']})
    if(user):
      print('User Exist')
      return jsonify({'ok': False, 'message': 'User with above Email already Exists!'}), 200
    else:
      name=data['name']
      email=data['email']
      password=data['password']
      hashpass = generate_password_hash(password, method='sha256')
      if request.method == 'POST':
          if data.get('name', None) is not None and data.get('email', None) is not None:
              db.userLogin({name:name,email:email,password:hashpass})
              return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
          else:
              return jsonify({'ok': False, 'message': 'Something went wrong!'}), 200

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
      data=request.json
      check_user = db.userLogin.find_one({'email': data['email']})
      if check_user:
          if check_password_hash(check_user['password'],data['password']):
              login_user=check_user
              return jsonify({'ok': False, 'message': 'Logged in!!!!'}), 200
      return jsonify({'ok': False, 'message': 'Email or password not matching, please try again!'}), 200
    return jsonify({'ok': False, 'message': 'Something went wrong!'}), 200


if __name__ == '__main__':
    app.run(debug = True )
